<?php

function mdc_form_system_theme_settings_alter(&$form, \Drupal\Core\Form\FormStateInterface &$form_state, $form_id = NULL, $theme = '') {

	// Work-around for a core bug affecting admin themes. See issue #943212.
	if (isset($form_id)) {
		return;
	}

	$form['mdc'] = [
		'#type' => 'details',
		'#title' => t('MDC Settings'),
		'#open' => TRUE,
		'#weight' => -1,
	];

	$form['mdc']['mdc_footer'] = [
		'#type' => 'checkbox',
		'#title' => t('Show MDC admin footer'),
		'#default_value' => theme_get_setting('mdc_footer'),
		'#tree' => FALSE,
	];

}