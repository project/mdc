<?php

/**
 * @file
 * Lists available colors and color schemes for the Bartik theme.
 */

$info = [
	// Available colors and color labels used in theme.
	'fields' => [
		'base' => t('Primary'),
		'secondary' => t('Secondary'),
    'link' => t('Text on Primary'),
    'text' => t('Text on Secondary'),
	],
	// Pre-defined color schemes.
	'schemes' => [
		'default' => [
			'title' => t('Slate (Default)'),
			'colors' => [
				'base' => '#546e7a',
				'secondary' => '#ff6d00',
        // mandatory fields
        'link' => '#ffffff',
        'text' => '#ffffff'
			],
		],
		'seaside' => [
			'title' => t('Seaside'),
			'colors' => [
        'base' => '#757575',
        'secondary' => '#00e5ff',
        'link' => '#ffffff',
        'text' => '#ffffff'
			],
		],
    'chocolate_mint' => [
      'title' => t('Chocolate Mint'),
      'colors' => [
        'base' => '#6d4c41',
        'secondary' => '#a5d6a7',
        'link' => '#ffffff',
        'text' => '#000000',
      ],
    ],
    'murica' => [
      'title' => t('Murica'),
      'colors' => [
        'base' => '#1976d2',
        'secondary' => '#dd2c00',
        'link' => '#ffffff',
        'text' => '#ffffff',
      ],
    ],
	],

	// CSS files (excluding @import) to rewrite with new color scheme.
	'css' => [
		'css/colors.css',
	],

	// Gradient definitions.
	'gradients' => [
		[
			// (x, y, width, height).
			'dimension' => [0, 0, 0, 0],
			// Direction of gradient ('vertical' or 'horizontal').
			'direction' => 'vertical',
			// Keys of colors to use for the gradient.
			'colors' => ['top', 'bottom'],
		],
	],

	// Preview files.
//	'preview_library' => 'mdc/color.preview',
//	'preview_html' => 'color/preview.html',
	'preview_css' => 'color/preview.css',

];
